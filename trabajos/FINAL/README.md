# Easyfastkey

Easyfastkey es una aplicación basada en bash para utilizar únicamente en terminales linux. Sirve para realizar gestión, almacenamiento y generación de nuestras contraseñas.

## Instrucciones

El programa es user friendly, no deberían tener nada en cuenta mas que seguir los pasos en pantalla para realizar las acciones deseadas.  

## Funciones

Podemos realizar dentro de Easyfastkey las siguientes acciones:
```bash
* Ingresar nuestros usuarios indicando la plataforma de donde provienen y generarles 
  contraseñas seguras en un listado generado por el programa.(Con un máximo de 10 entradas)
* Cambiar la contraseña de un usuario ya ingresado
* Modificar los usuarios o plataformas ya ingresados
* Eliminar usuarios ingresados
* Listar todos los usuarios
```

## Explicación de las partes fundamentales del código script

```bash
Para la generación de clave usamos el siguiente código dividido por las posibilidades
de longitud de clave en un case :

Linea 42:
           CARACTERES="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
            echo "Clave de $USUARIO :" >> /etc/easyfastkey/contraseñas
            for i in {1..6} ; do
            echo -n "${CARACTERES:RANDOM%${#CARACTERES}:1}" >> /etc/easyfastkey/contraseñas
            done

Básicamente declaramos los caracteres posibles en la nueva contraseña en la variable "CARACTERES"
Utilizamos for i in para indicar la cantidad de caracteres que preciso para la contraseña, en este caso son 6.
luego utilizo echo -n para indicando las variables de caracteres y usando la variable del sistema $RANDOM para
que me genere una contraseña al azar y la guarde en la ruta /etc/easyfastkey/contraseñas.

Linea 164:

sed -i "6s/.*/$NUEVACLAVE/" /etc/easyfastkey/contraseñas

Utilizamos sed para remplazar la linea del archivo que indicamos con case anteriormente.
En este caso remplazaría la contraseña del primer usuario que se encuentra en la linea
6 por otra que nosotros le indiquemos en input que se encuentra anteriormente.

Linea 249:
      sed -i '15,21d' /etc/easyfastkey/contraseñas
      cat /etc/easyfastkey/contraseñas
      echo "Se elimino el usuario correctamente"

Aquí hacemos algo muy similar que lo mostrado anteriormente.
En este caso usamos sed para eliminar el usuario numero 3 que se encontrara desde la linea 15 hasta las 21 de
nuestro archivo.

```

## Creador

Brandon Zalazar para la clase de programacion de Sergio Pernas.
ISTEA.

## GitLab
[Git Lab Brandon Zalazar](https://gitlab.com/brandon.zalazar/brandon-zalazar/-/tree/main)
