#!/bin/bash

if [ $UID -ne 0 ]; then
		echo "Debe ser root para acceder a este script"
		exit
fi


echo "Programa de gestion y generacion de claves EasyFastKey"
echo "1.Si es la primera vez que ingresa al programa, ingrese esta opcion para generar el directorio de la misma"
echo "2.Ingresar nuevo usuario con contraseña"
echo "3.Cambiar contraseña de un usuario especifico"
echo "4.Modificar usuario o plataforma"
echo "5.Eliminar usuario"
echo "6.Listar usuarios"
read -p "" INTRO

  if [ $INTRO = 1 ]; then
    mkdir /etc/easyfastkey
    echo "Se creo el directorio correctamente, vuelva a iniciar el programa para comenzar"
    echo "a gestionar sus contraseñas"
    exit
  fi

  if [ $INTRO = 2 ]; then
        echo "Ingrese la plataforma para la cual esta creando este usuario/contraseña"
        read -p "" PLATAFORMA
        echo "Ingrese el nombre de usuario"
        read -p "" USUARIO
        echo "Ingrese la cantidad de caracteres que desee para la clave(debe ser entre 6 y 16 caracteres)"
        read -p "" LARGO
        echo "Usuario:" >> /etc/easyfastkey/contraseñas
        echo "$USUARIO" >> /etc/easyfastkey/contraseñas
        echo "Plataforma:" >> /etc/easyfastkey/contraseñas
        echo "$PLATAFORMA" >> /etc/easyfastkey/contraseñas

        
          case $LARGO in
            6)
            CARACTERES="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
            echo "Clave de $USUARIO :" >> /etc/easyfastkey/contraseñas
            for i in {1..6} ; do
            echo -n "${CARACTERES:RANDOM%${#CARACTERES}:1}" >> /etc/easyfastkey/contraseñas
            done
            echo "" >> /etc/easyfastkey/contraseñas
            echo "" >> /etc/easyfastkey/contraseñas
            echo "El usuario se guardo correctamente en su base de datos"            
            ;;
            7)
            CARACTERES="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
            echo "Calve de $USUARIO :" >> /etc/easyfastkey/contraseñas
            for i in {1..7} ; do
            echo -n "${CARACTERES:RANDOM%${#CARACTERES}:1}" >> /etc/easyfastkey/contraseñas
            done
            echo "" >> /etc/easyfastkey/contraseñas
            echo "" >> /etc/easyfastkey/contraseñas
            echo "El usuario se guardo correctamente en su base de datos"
            ;;
            8)
            CARACTERES="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
            echo "Clave de $USUARIO :" >> /etc/easyfastkey/contraseñas
            for i in {1..8} ; do
            echo -n "${CARACTERES:RANDOM%${#CARACTERES}:1}" >> /etc/easyfastkey/contraseñas
            done
            echo "" >> /etc/easyfastkey/contraseñas
            echo "" >> /etc/easyfastkey/contraseñas
            echo "El usuario se guardo correctamente en su base de datos"
            ;;
            9)
            CARACTERES="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
            echo "Clave de $USUARIO :" >> /etc/easyfastkey/contraseñas
            for i in {1..9} ; do
            echo -n "${CARACTERES:RANDOM%${#CARACTERES}:1}" >> /etc/easyfastkey/contraseñas
            done
            echo "" >> /etc/easyfastkey/contraseñas
            echo "" >> /etc/easyfastkey/contraseñas
            echo "El usuario se guardo correctamente en su base de datos"
            ;;
            10)
            CARACTERES="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
            echo "Clave de $USUARIO :" >> /etc/easyfastkey/contraseñas
            for i in {1..10} ; do
            echo -n "${CARACTERES:RANDOM%${#CARACTERES}:1}" >> /etc/easyfastkey/contraseñas
            done
            echo "" >> /etc/easyfastkey/contraseñas
            echo "" >> /etc/easyfastkey/contraseñas
            echo "El usuario se guardo correctamente en su base de datos"
            ;;
            11)
            CARACTERES="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
            echo "Clave de $USUARIO :" >> /etc/easyfastkey/contraseñas
            for i in {1..11} ; do
            echo -n "${CARACTERES:RANDOM%${#CARACTERES}:1}" >> /etc/easyfastkey/contraseñas
            done
            echo "" >> /etc/easyfastkey/contraseñas
            echo "" >> /etc/easyfastkey/contraseñas
            echo "El usuario se guardo correctamente en su base de datos"
            ;;
            12)
            CARACTERES="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
            echo "Clave de $USUARIO :" >> /etc/easyfastkey/contraseñas
            for i in {1..12} ; do
            echo -n "${CARACTERES:RANDOM%${#CARACTERES}:1}" >> /etc/easyfastkey/contraseñas
            done
            echo "" >> /etc/easyfastkey/contraseñas
            echo "" >> /etc/easyfastkey/contraseñas
            echo "El usuario se guardo correctamente en su base de datos"
            ;;
            13)
            CARACTERES="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
            echo "Clave de $USUARIO :" >> /etc/easyfastkey/contraseñas
            for i in {1..13} ; do
            echo -n "${CARACTERES:RANDOM%${#CARACTERES}:1}" >> /etc/easyfastkey/contraseñas
            done
            echo "" >> /etc/easyfastkey/contraseñas
            echo "" >> /etc/easyfastkey/contraseñas
            echo "El usuario se guardo correctamente en su base de datos"
            ;;
            14)
            CARACTERES="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
            echo "Clave de $USUARIO :" >> /etc/easyfastkey/contraseñas
            for i in {1..14} ; do
            echo -n "${CARACTERES:RANDOM%${#CARACTERES}:1}" >> /etc/easyfastkey/contraseñas
            done
            echo "" >> /etc/easyfastkey/contraseñas
            echo "" >> /etc/easyfastkey/contraseñas
            echo "El usuario se guardo correctamente en su base de datos"
            ;;
            15)
            CARACTERES="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
            echo "Clave de $USUARIO :" >> /etc/easyfastkey/contraseñas
            for i in {1..15} ; do
            echo -n "${CARACTERES:RANDOM%${#CARACTERES}:1}" >> /etc/easyfastkey/contraseñas
            done
            echo "" >> /etc/easyfastkey/contraseñas
            echo "" >> /etc/easyfastkey/contraseñas
            echo "El usuario se guardo correctamente en su base de datos"
            ;;
            16)
            CARACTERES="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
            echo "Clave de $USUARIO :" >> /etc/easyfastkey/contraseñas
            for i in {1..16} ; do
            echo -n "${CARACTERES:RANDOM%${#CARACTERES}:1}" >> /etc/easyfastkey/contraseñas
            done
            echo "" >> /etc/easyfastkey/contraseñas
            echo "" >> /etc/easyfastkey/contraseñas
            echo "El usuario se guardo correctamente en su base de datos"
            ;;            
            *)
            echo "Opcion invalida, vuelva a intentarlo."
            exit
            ;;
          esac
  fi

  if [ $INTRO = 3 ]; then
    echo "Ingrese el numero de orden del usuario que desea modificar su clave"
    cat /etc/easyfastkey/contraseñas
    read -p "" MODIFICARCLAVE
      case $MODIFICARCLAVE in
        1)
        echo "Escriba la nueva clave"
        read -p "" NUEVACLAVE
        sed -i "6s/.*/$NUEVACLAVE/" /etc/easyfastkey/contraseñas
        echo "Se cambio la clave exitosamente"
        ;;
        2)
        echo "Escriba la nueva clave"
        read -p "" NUEVACLAVE
        sed -i "13s/.*/$NUEVACLAVE/" /etc/easyfastkey/contraseñas
        echo "Se cambio la clave exitosamente"
        ;;
        3)
        echo "Escriba la nueva clave"
        read -p "" NUEVACLAVE
        sed -i "20s/.*/$NUEVACLAVE/" /etc/easyfastkey/contraseñas
        echo "Se cambio la clave exitosamente"
        ;;
        4)
        echo "Escriba la nueva clave"
        read -p "" NUEVACLAVE
        sed -i "27s/.*/$NUEVACLAVE/" /etc/easyfastkey/contraseñas
        echo "Se cambio la clave exitosamente"
        ;;
        5)
        echo "Escriba la nueva clave"
        read -p "" NUEVACLAVE
        sed -i "34s/.*/$NUEVACLAVE/" /etc/easyfastkey/contraseñas
        echo "Se cambio la clave exitosamente"
        ;;
        6)
        echo "Escriba la nueva clave"
        read -p "" NUEVACLAVE
        sed -i "41s/.*/$NUEVACLAVE/" /etc/easyfastkey/contraseñas
        echo "Se cambio la clave exitosamente"
        ;;
        7)
        echo "Escriba la nueva clave"
        read -p "" NUEVACLAVE
        sed -i "48s/.*/$NUEVACLAVE/" /etc/easyfastkey/contraseñas
        echo "Se cambio la clave exitosamente"
        ;;
        8)
        echo "Escriba la nueva clave"
        read -p "" NUEVACLAVE
        sed -i "55s/.*/$NUEVACLAVE/" /etc/easyfastkey/contraseñas
        echo "Se cambio la clave exitosamente"
        ;;
        9)
        echo "Escriba la nueva clave"
        read -p "" NUEVACLAVE
        sed -i "62s/.*/$NUEVACLAVE/" /etc/easyfastkey/contraseñas
        echo "Se cambio la clave exitosamente"
        ;;
        10)
        echo "Escriba la nueva clave"
        read -p "" NUEVACLAVE
        sed -i "69s/.*/$NUEVACLAVE/" /etc/easyfastkey/contraseñas
        echo "Se cambio la clave exitosamente"
        ;;
      esac
  fi

  if [ $INTRO = 4 ]; then
    echo "Ingrese el nombre del usuario o plataforma que desea modificar"
    read -p "" USUARIOMODIFICAR
    echo "Ingrese el nuevo nombre de usuario o plataforma"
    read -p "" NUEVOUSUARIO
    sed -i "s/$USUARIOMODIFICAR/$NUEVOUSUARIO/g" /etc/easyfastkey/contraseñas
  fi


  if [ $INTRO = 5 ]; then
    echo "Ingrese el numero de orden del usuario que desea eliminar"
    cat /etc/easyfastkey/contraseñas
    read -p "" ELIMINARUSUARIO
    case $ELIMINARUSUARIO in
      1)
      sed -i '1,7d' /etc/easyfastkey/contraseñas
      cat /etc/easyfastkey/contraseñas
      echo "Se elimino el usuario correctamente"
      ;;
      2)
      sed -i '8,14d' /etc/easyfastkey/contraseñas
      cat /etc/easyfastkey/contraseñas
      echo "Se elimino el usuario correctamente"
      ;;
      3)
      sed -i '15,21d' /etc/easyfastkey/contraseñas
      cat /etc/easyfastkey/contraseñas
      echo "Se elimino el usuario correctamente"
      ;;
      4)
      sed -i '22,28d' /etc/easyfastkey/contraseñas
      cat /etc/easyfastkey/contraseñas
      echo "Se elimino el usuario correctamente"
      ;;
      5)
      sed -i '29,35d' /etc/easyfastkey/contraseñas
      cat /etc/easyfastkey/contraseñas
      echo "Se elimino el usuario correctamente"
      ;;
      6)
      sed -i '36,42d' /etc/easyfastkey/contraseñas
      cat /etc/easyfastkey/contraseñas
      echo "Se elimino el usuario correctamente"
      ;;
      7)
      sed -i '43,49d' /etc/easyfastkey/contraseñas
      cat /etc/easyfastkey/contraseñas
      echo "Se elimino el usuario correctamente"
      ;;
      8)
      sed -i '50,56d' /etc/easyfastkey/contraseñas
      cat /etc/easyfastkey/contraseñas
      echo "Se elimino el usuario correctamente"
      ;;
      9)
      sed -i '57,63d' /etc/easyfastkey/contraseñas
      cat /etc/easyfastkey/contraseñas
      echo "Se elimino el usuario correctamente"
      ;;
      10)
      sed -i '64,70d' /etc/easyfastkey/contraseñas
      cat /etc/easyfastkey/contraseñas
      echo "Se elimino el usuario correctamente"
      ;;
    esac   
  fi

  if [ $INTRO = 6 ]; then
    cat /etc/easyfastkey/contraseñas
    exit
  fi
