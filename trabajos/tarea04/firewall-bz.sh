#!/bin/bash

if [ $UID -ne 0 ]; then
		echo "Debe ser root para acceder a este script"
		exit
fi

echo "Seleccione la opcion deseada"
echo "1.Cambiar politicas de las cadenas"
echo "2.Listar reglas"
echo "3.Permitir/denegar reglas"
echo "4.Mover de posicion reglas"
echo "5.Backups de reglas"
echo "6.Permitir/denegar que el firewall se cargue con el sistema"
echo "7.Restablecer configuraciones"
read -p "" INTRO

#Presentamos las opciones al usuario y utilizamos el comando read para que el mismo pueda elegir la que desee.

	if [ $INTRO = 1 ]; then
		echo "¿Que desea cambiar?"
		echo "1.Permitir todo en INPUT"
		echo "2.Bloquear todo en INPUT"
		echo "3.Permitir todo en FORWARD"
		echo "4.Bloquear todo en FORWARD"
		echo "5.Permitir todo en OUTPUT"
		echo "6.Bloquear todo en OUTPUT"
		echo "7.Restablecer configuraciones"
		read -p "" POLITICA
			if [ $POLITICA = 1 ]; then
				iptables -P INPUT ACCEPT
				echo "Se cambio el valor de la politica:"
				iptables -L
				exit
			fi

			if [ $POLITICA = 2 ]; then
				iptables -P INPUT DROP
				echo "Se cambio el valor de la politica:"
				iptables -L
				exit
			fi

			if [ $POLITICA = 3 ]; then
				iptables -P FORWARD ACCEPT
				echo "Se cambio el valor de la politica:"
				iptables -L
				exit
			fi

			if [ $POLITICA = 4 ]; then
				iptables -P FORWARD DROP
				echo "Se cambio el valor de la politica:"
				iptables -L
				exit
			fi	

			if [ $POLITICA = 5 ]; then
				iptables -P OUTPUT ACCEPT
				echo "Se cambio el valor de la politica:"
				iptables -L
				exit
			fi	

			if [ $POLITICA = 6 ]; then
				iptables -P OUTPUT DROP
				echo "Se cambio el valor de la politica:"
				iptables -L
				exit
			fi	
	fi

#Damos las opciones para poder cambiar las politicas de las cadenas.

	if [ $INTRO = 2 ]; then
		echo "Desea ver las reglas como:"
		echo "1.Especificacion"
		echo "2.Tablas"
		echo "3.Tablas y con numeracion"
		echo "4.Tabla con los puertos en formato numerico"
		read -p "" LISTAREGLAS

			if [ $LISTAREGLAS = 1 ]; then
				echo "Se muestran las reglas mediante especificacion a continuacion:"
				iptables -S
				exit
			fi

			if [ $LISTAREGLAS = 2 ]; then
				echo "Se muestran las reglas como tablas a continuacion:"
				iptables -L
				exit
			fi

			if [ $LISTAREGLAS = 3 ]; then
				echo "Se muestran las reglas como tablas y con numeracion a continuacion:"
				iptables -L --line-numbers
				exit
			fi

			if [ $LISTAREGLAS = 4 ]; then
				echo "Se muestran las reglas como tablas con puertos en formato numerico a continuacion:"
				iptables -L -n
				exit
			fi
	fi

	if [ $INTRO = 3 ]; then
		echo "1.Permitir conexiones de reglas"
		echo "2.Denegar conexiones de reglas"
		read -p "" REGLAS
		if [ $REGLAS = 1 ]; then
			echo "1.Permitir conexiones HTTP(80)"
			echo "2.Permitir conexiones SSH(22)"
			echo "3.Permitir paquetes entrantes que sean respuestas de paquetes salientes"
			read -p "" PERMITIR
				if [ $PERMITIR = 1 ]; then
					iptables -A INPUT -p tcp --dport 80 -j ACCEPT
					echo "se permitio las conexiones HTTP"
					iptables -L
					exit
				fi

				if [ $PERMITIR = 2 ]; then
					iptables -A INPUT -p tcp --dport 22 -j ACCEPT
					echo "se permitio las conexiones SSH"
					iptables -L
					exit
				fi

				if [ $PERMITIR = 3 ]; then
					iptables -A INPUT -m state --state RELATED, STABLISHED -j ACCEPT
					echo "Se permitio paquetes entrantes que sean respuestas de paquetes salientes"
					iptables -L
					exit
				fi
		fi
	
		if [ $REGLAS = 2 ]; then
			echo "1.Denegar conexiones HTTP(80)"
			echo "2.Denegar conexiones SSH(22)"
			echo "3.Denegar paquetes entrantes que sean respuestas de paquetes salientes"
			read -p "" DENEGAR
				if [ $DENEGAR = 1 ]; then
					iptables -A INPUT -p tcp --dport 80 -j DROP
					echo "se denego las conexiones HTTP"
					iptables -L
					exit
				fi

				if [ $DENEGAR = 2 ]; then
					iptables -A INPUT -p tcp --dport 22 -j DROP
					echo "se denego las conexiones SSH"
					iptables -L
					exit
				fi

				if [ $DENEGAR = 3 ]; then
					iptables -A INPUT -m state --state RELATED, STABLISHED -j DROP
					echo "Se denego paquetes entrantes que sean respuestas de paquetes salientes"
					iptables -L
					exit
				fi
		fi
	fi

	if [ $INTRO = 4 ]; then
		iptables -L -n
		echo "Escriba la cadena direccion ip y la politica de la regla a modificar"
		echo "Cadena:"
		read -p "" CADENA
		echo "Ip:"
		read -p "" IP
		echo "Politica:"
		read -p "" POLITICA
		echo "Ahora ingrese en formato numerico la nuevo posicion donde desea moverlo"
		read -p "" POSICION 
		iptables -I $CADENA $POSICION -s $IP -j $POLITICA
		echo "Se cambio de posicion la regla indicada."
		iptables -L -n
		exit
	fi

	if [ $INTRO = 5 ]; then
		echo "Desea:"
		echo "1.guardar reglas"
		echo "2.restuarar reglas"
		read -p "" BACKUP
			if [ $BACKUP = 1 ]; then
				echo "Elige el nombre del fichero que contenga las reglas a guardar(debe terminar en .txt)"
				read -p "" NOMBREREGLAS
				iptables-save > /root/$NOMBREREGLAS
				echo "Se guardo correctamente el backup de reglas"
				exit
			fi

			if [ $BACKUP = 2 ]; then
				echo "Introduzca el nombre del archivo a respaldar(debe terminar en .txt)"
				read -p "" NOMBREREGLAS
				iptables-restore < /root/$NOMBREREGLAS
				echo "se respaldo correctamente al backup de reglas"
				exit
			fi
	fi

	if [ $INTRO = 6 ]; then
		echo "Seleccione la opcion que desee:"
		echo "1.Activar Firewall en su sistema"
		echo "2.Desactivar Firewall de su sistema"
		read -p "" FIREWALL
			if [ $FIREWALL = 1 ]; then
				service iptables start
				exit
			fi
			if [ $FIREWALL = 2 ]; then
				service iptables stop
				exit
			fi

	fi

	if [ $INTRO = 7 ]; then
		echo "¿Que desea restablecer?"
		echo "1.Configuracion del firewall"
		echo "2.Vaciar todas las cadenas"
		read -p "" RESTABLECER
			if [ $RESTABLECER = 1 ]; then
				echo "¿Esta seguro que desea restablecer dicha configuracion?Escriba SI para continuar"
				read -p "" CONFIRMACION
					if [ $CONFIRMACION = SI ]; then
						iptables -F
						echo "se restablecio la configuracion del firewall"
						exit
					fi
			fi		
			if [ $RESTABLECER = 2 ]; then
				echo "¿Esta seguro que desea restablecer dicha configuracion?Escriba SI para continuar"
				read -p "" CONFIRMACION
					if [ $CONFIRMACION = SI ]; then
						iptables -F
						echo "se vaciaron todas las cadenas"
						exit
					fi
			fi		
	fi


