#!/bin/bash

EXITSTATUS=$?

#declaro la variable EXITSTATUS que utilizare en los scripts para que me devuelva el resultado del mismo

echo "Seleccione el script que desea ejecutar"
echo "1.Cambiar permisos de archivos/directorios"
echo "2.Cambiar propietario de archivos/directorios"
echo "3.Crear, eliminar o ver grupos"
read -p "" INTRO

#Presentamos las opciones al usuario y utilizamos el comando read para que el mismo pueda elegir una de las 3, que luego con la variable INTRO dependiendo su seleccion... 
#...llamara al script requerido mediante la condicional if

	if [ $INTRO = 1 ]; then
		echo "Script para cambiar permisos rwx de archivos y directorios"
		read -p "Introduzca la ruta del archivo o directorio a modificar: " RUTA

		ls -l $RUTA

		read -p "Nombre del fichero/carpeta: " ARCHIVO

		read -p "Ingrese el permiso deseado: " PERMISOS

		echo "Ruta seleccionada: "$RUTA "fichero/carpeta" $ARCHIVO

		cd $RUTA
		chmod -v "$PERMISOS" "$ARCHIVO"
		echo "El exit status de este script es: $EXITSTATUS"
		exit
	fi

#En este primer script utilizamos tambien read para que el usuario indique la ruta del archivo ,el archivo en si el cual quiere modificar sus permisos y que permisos quiere otorgarle.
#Luego con el comanddo chmod cumplis lo que el usuario requiera. Tambien vale aclarar que siempre que utilizemos la condicional if hay que cerrarla con fi.
#Tambien al final con echo reflejamos el exit status del script, esto lo aplicaremos en todos los scripts.

	if [ $INTRO = 2 ]; then
		echo "script para cambiar propietario y grupo de ficheros"

		read -p "Introduzca la ruta del archivo o directorio a modificar: " RUTA2

		ls -r $RUTA2

		read -p "Nombre del fichero/carpeta: " ARCHIVO2

		read -p "Ingrese el nombre del nuevo propietario: " PROPIETARIO

		echo "Ruta seleccionada: "$RUTA2 "fichero/carpeta" $ARCHIVO2 "Nuevo propietario: " $PROPIETARIO

		cd $RUTA2
		chown -v $PROPIETARIO $ARCHIVO2
		echo "El exit status de este script es: $EXITSTATUS"
		exit
	fi

#En este script hacemos algo muy similar al anterior solo que esta vez utilizamos el comando chown para cambiar el propietario.

	if [ $INTRO = 3 ]; then
		echo "Script para crear,eliminar o ver grupos"

		if [ $UID -ne 0 ]; then
		echo "Debe ser root para acceder a este script"
		exit
		fi

			echo "¿Desea agregar, eliminar o ver los grupos existentes?"
			read -p  "" OPCION

			if [ $OPCION = agregar ]; then
				read -p "Ingrese el nombre del nuevo grupo: " NOMBREGRUPO
				addgroup $NOMBREGRUPO
				echo "El exit status de este script es: $EXITSTATUS"
				exit
			fi

			if [ $OPCION = eliminar ]; then
				read -p "Ingrese el nombre del grupo que desea eliminar " NOMBREGRUPO
				delgroup $NOMBREGRUPO
				echo "El exit status de este script es: $EXITSTATUS"
				exit
			fi

			if [ $OPCION = ver ]; then
				echo "Se lista a continuacion la lista de grupos:"
				cat /etc/group
				echo "El exit status de este script es: $EXITSTATUS"
				exit
			fi

#En este ultimo script decidi dividirlo en 3 condicionales por cada opcion que el usuario quiera realizar (crear,eliminar o ver los grupos)
#Antes que se ejecuten una de esas 3, ingrese una condicional que indica con la variable del sistema $UID es -ne 0 (no es un usuario root) no podra acceder a este script.
#Utilizamos addgroup para agregar un nuevo grupo, delgroup para eliminar uno existente y el comando cat a la ruta /etc/group para que liste los grupos.
	fi


