#!/bin/bash

mkdir /tmp/backup

#Creamos el directorio donde se alojaran los backups

LOGS=/tmp/backup/backup.log

#Definimos donde se guardara el fichero de logs

directorios_backup=/home/$USER

#Declaramos los directorios a respaldar

destino=/tmp/backup

#Indicamos el destino de los backpus

fecha=$(date +"%d-%m-%Y_%H-%M-%S")

nombre_archivo=respaldo_$fecha.tgz

#Indicamos el nombre del archivo que en este caso sera la fecha del momento el cual lo creemos

echo "Realizando backup del usuario $USER dentro de /tmp/backup"

tar czf $destino/$nombre_archivo $directorios_backup  2> $LOGS

#Imprimimos en pantalla que el proceso de backup comienza y con el comando tar realizamos el backup en un comprimido, tambien con 2> enviara los errores a la variable LOGS en el archivo que especificamos

echo "Se finalizo el backup del usuario $USER en /tmp/backup"

#Finalizamos con un mensaje el cual indica que se termino de realizar el backup
