#!/bin/bash

if [ $UID -ne 0 ]; then
		echo "Debe ser root para acceder a este script"
		exit
fi

echo "Seleccione la opcion deseada"
echo "1.Modificar la ip y hostname del servidor"
echo "2.Instalación/desinstalacion de componentes de un stack"
echo "3.Alta/baja de sitios"
echo "4.Instalar, configurar y hacer deploys de sitios con Wordpress"
echo "5.Migrar sitios"
read -p "" INTRO

	if [ $INTRO = 1 ]; then
		echo "¿Que desea modificar?"
		echo "1.Hostname"
		echo "2.Ip"
		read -p "" HOSTIP
			if [ $HOSTIP = 1 ]; then
				echo "Escriba a continucion el nuevo hostname"
				read -p "" HOSTNAME
				hostnamectl set-hostname $HOSTNAME
				echo "Se cambio el hostname por $HOSTNAME . Es necesario reiniciar el equipo para aplicar cambios, desea reiniciarlo ahora? (y/n)"
				read -p "" RESET
					if [ $RESET = y ]; then
						echo "Reiniciando..."
						reboot
						exit
					fi
							while [[ $RESET = n ]]; do
								echo "No olvides reiniciar luego para aplicar cambios."
								exit
							done

 			fi

 			if [ $HOSTIP = 2 ]; then
 				echo "** Advertencia: Si cambia la ip conectado remotamente a este equipo se desconectara su sesion ***"
 				echo "Digite su nueva ip:"
 				read -p "" ADDRESS
 				echo "Digite su nueva mascara de red:"
 				read -p "" NETMASK
 				echo "Digite su nuevo default gateway:"
 				read -p "" GATEWAY
 				sed -i "s/iface enp0s3 inet dhcp/iface enp0s3 inet static/" /etc/network/interfaces
 				echo -e "address $ADDRESS  \nnetmask $NETMASK \ngateway $GATEWAY" >> /etc/network/interfaces
 				echo "Aplicando cambios..."
 				ifdown enp0s3
 				ifup enp0s3
 				echo "Se cambio la ip solicitada($ADDRESS)"
 			fi
 	fi			

 	 if [ $INTRO = 2 ]; then
 		echo "¿Que desea hacer?"
 		echo "1.Instalar servidor web Apache"
 		echo "2.Desinstalar servidor web Apache"
 		echo "3.Instalar Stack php y mysql"
 		echo "4.desinstalar Stack php y mysql"
 		read -p "" INSTALACIONES

 			if [ $INSTALACIONES = 1 ]; then
 				apt update && apt install apache2
 				exit
 			fi	
 				while [[ $INSTALACIONES = 2 ]];do 
 					systemctl stop apache2 
 					apt-get purge apache2 apache2-utils apache2-bin apache2.2-common
 					apt-get apt-get autoremove
 					echo "Se desinstalo servidor web apache"
 				done
 			if [ $INSTALACIONES = 3 ]; then
 				apt install default-mysql-server php libapache2-mod-php php-mysql
 				systemctl restart apache2
 				exit
 			fi		
 				while [[ $INSTALACIONES = 4 ]];do 
 					service apache2 stop
 					apt-get purge mysql-server apache2 php5
 					apt-get remove libapache2-mod-php5 libapr1 libaprutil1 libdbd-mysql-perl libdbi-perl libmysqlclient15off libnet-daemon-perl libplrpc-perl libpq5 mysql-client-5.0 mysql-common mysql-server mysql-server-5.0 php5-common php5-mysql
 					echo "Se desinstalo stack php y mysql"
 					exit
 				done
	fi

	if [ $INTRO = 3 ]; then
		echo "¿Desea dar de alta o baja un sitio?"
		echo "1.Alta"
		echo "2.Baja"
		read -p "" ALTAOBAJA
			if [ $ALTAOBAJA = 1 ]; then
				echo "Ingrese el nombre del nuevo sitio, debe terminar en .com (ejemplo.com)"
				read -p "" NUEVOSITIO
				echo "¿Desea que los logs del nuevo sitio se guarden en el archivo predeterminado o en nuevo para este site?"
				echo "1.Por defecto"
				echo "2.Nuevo"
				read -p "" RUTALOGS
					if [ $RUTALOGS = 1 ]; then	
						echo -e "<VirtualHost *:80>  \n \n	ServerAdmin webmaster@localhost \n	DocumentRoot /var/www/$NUEVOSITIO \n	servername $NUEVOSITIO \n	ErrorLog /var/log/apache2/error.log \n	CustomLog /var/log/apache2/access.log combined
					\n</VirtualHost>" >> /etc/apache2/sites-available/$NUEVOSITIO.conf
						mkdir /var/www/$NUEVOSITIO
						echo "<h1>Hola este es tu nuevo sitio $NUEVOSITIO </h1>" > /var/www/$NUEVOSITIO/index.html
						echo "Activando sitio"
						a2ensite $NUEVOSITIO.conf
						systemctl reload apache2
						exit
					fi
						while [[ $RUTALOGS = 2 ]]; do
							echo -e "<VirtualHost *:80>  \n \n	ServerAdmin webmaster@localhost \n	DocumentRoot /var/www/$NUEVOSITIO \n	servername $NUEVOSITIO \n	/var/log/apache2/$NUEVOSITIO.error.log \n	CustomLog /var/log/apache2/$NUEVOSITIO.log combined
						\n</VirtualHost>" >> /etc/apache2/sites-available/$NUEVOSITIO.conf
							echo "Se creo un nuevo archivo para este sitio"
							mkdir /var/www/$NUEVOSITIO
							echo "<h1>Hola este es tu nuevo sitio $NUEVOSITIO </h1>" > /var/www/$NUEVOSITIO/index.html
							echo "Activando sitio"
							a2ensite $NUEVOSITIO.conf
							systemctl reload apache2
							exit
						done

			fi	

			while [[ $ALTAOBAJA = 2 ]]; do
				echo "Ingrese el nombre del sitio que desea dar de baja, debe terminar en .com (ejemplo.com) "
				read -p "" BAJASITIO
				a2dissite $BAJASITIO.conf
				rm -rf /var/www/$BAJASITIO
				rm -i /etc/apache2/sites-available/$BAJASITIO.conf
				systemctl reload apache2
				echo "Se dio de baja el sitio correctamente"
				exit
			done
	fi

	if [ $INTRO = 4 ]; then
		echo "¿Que desea hacer?"
		echo "1.Crear base de datos wordpress con usuario con privilegios sobre la misma "
		echo "2.Descargar e instalar wordpress"
		echo "3.Configurar wordpress"

		read -p "" HACERWORD
		if [ $HACERWORD = 1 ]; then
			mysql -e "CREATE DATABASE wordpress;"
			mysql -e "GRANT ALL ON wordpress.* TO 'user'@'localhost' IDENTIFIED BY '1234';"
			mysql -e "FLUSH PRIVILEGES;"
			echo "Usuario creado con contraseña default 1234"
			exit
		fi

		while [[ $HACERWORD = 2 ]]; do
			wget -P /var/www/html https://wordpress.org/latest.tar.gz
			echo "Descomprimiendo archivo..."
			cd /var/www/html
			tar -xf latest.tar.gz
			rm -r latest.tar.gz
			cd wordpress
			cp wp-config-sample.php wp-config.php
			exit
		done
		if [ $HACERWORD = 3 ]; then
			cd /var/www/html/wordpress
			sed -i "s/database_name_here/wordpress/" /var/www/html/wordpress/wp-config.php
			sed -i "s/username_here/user/" /var/www/html/wordpress/wp-config.php
			sed -i "s/password_here/1234/" /var/www/html/wordpress/wp-config.php
			echo "Configuracion de wordpress finalizada"
			echo "Para seguir configurando wordpress ahora puede abrir de la siguiente forma desde su navegador http://<ipdelservidor>/wordpress"
		fi
	fi

	if [ $INTRO = 5 ]; then
		echo "Ingrese la ip del servidor a migrar"
		read -p "" IPSERVER
		echo "Ingrese el nombre del fichero vhost a migrar incluyendo .com (ejemplo.com)"
		read -p "" VHOSTMIGRAR
		scp /etc/apache2/sites-available/$VHOSTMIGRAR.conf root@<$IPSERVER>:/etc/apache2/sites-available
		scp -r /var/www/$VHOSTMIGRAR root@<$IPSERVER>:/var/www
		ssh root@<$IPSERVER> "a2ensite $VHOSTMIGRAR"
		ssh root@<$IPSERVER> 'systemctl reload apache2'
	fi




